Generate maker posters for Lpool MakeFest from the collected data and a template.

## Requires

 * Python — tested with 3.5.2,
 * LibreOffice — for ods/xlsx conversion
 * Inkscape — for SVG to PDF creation
 * pdftk (command line) — for pdf concatenation
